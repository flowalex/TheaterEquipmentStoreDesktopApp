﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WolfIT330DesktopApplication
{
    /// <summary>
    /// Interaction logic for WiredMics.xaml
    /// </summary>
    public partial class WiredMics : Window
    {
        public WiredMics()
        {
            InitializeComponent();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            int retval = 0;

            AppCompute appcomp = new AppCompute();

            string[] strinput = new string[6];
            int nbrinput = 0;
            string phantom = "";
            if (Yes1.IsChecked == true)
            { phantom = "Yes"; }
            else if (No1.IsChecked == true)
            { phantom = "No"; }

            string oprselected = "";

            if (Yes.IsChecked == true)
            { oprselected = "Yes"; }
            else if (No.IsChecked == true)
            { oprselected = "No"; }

            strinput[0] = WiredID.Text;
            strinput[1] = Brand.Text;
            strinput[2] = ProductName.Text;
            strinput[3] = Quantity.Text;
            strinput[4] = phantom;
            strinput[5] = oprselected;


            nbrinput = 6;

            retval = appcomp.showData(strinput, nbrinput);
        }
    }
}
