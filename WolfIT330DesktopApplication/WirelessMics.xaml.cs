﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WolfIT330DesktopApplication
{
    /// <summary>
    /// Interaction logic for WirelessMics.xaml
    /// </summary>
    public partial class WirelessMics : Window
    {
        public WirelessMics()
        {
            InitializeComponent();
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            int retval = 0;

            AppCompute appcomp = new AppCompute();

            string[] strinput = new string[6];
            int nbrinput = 0;

            string oprselected = "";

            if (Yes.IsChecked == true)
            { oprselected = "Yes"; }
            else if (No.IsChecked == true)
            { oprselected = "No"; }

            strinput[0] = WirelessID.Text;
            strinput[1] = Brand.Text;
            strinput[2] = Model.Text;
            strinput[3] = Type.Text;
            strinput[4] = Quantity.Text;
            strinput[5] = oprselected;
          

            nbrinput = 6;

            retval = appcomp.showData(strinput, nbrinput);
        }
    }
}
