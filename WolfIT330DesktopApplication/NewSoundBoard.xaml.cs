﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WolfIT330DesktopApplication
{
    /// <summary>
    /// Interaction logic for NewSoundBoard.xaml
    /// </summary>
    public partial class NewSoundBoard : Window
    {
        public NewSoundBoard()
        {
            InitializeComponent();
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            int retval = 0;

            AppCompute appcomp = new AppCompute();

            string[] strinput = new string[10];
            int nbrinput = 0;
            string selecteddate;
            selecteddate = createddate.SelectedDate.ToString();

            string oprselected = "";

            if (serviceYes.IsChecked == true)
            { oprselected = "Yes"; }
            else if(ServiceNo.IsChecked == true)
            { oprselected = "No"; }

            string digAna = "";

            if(Digital.IsChecked == true)
            { digAna = "Digital"; }
            else if (Analouge.IsChecked == true)
            { digAna = "Analouge"; }

            strinput[0] = txtSBoardID.Text;
            strinput[1] = txtSboardBrand.Text;
            strinput[2] = txtSboardModel.Text;
            strinput[3] = txtSboardQuantity.Text;
            strinput[4] = txtSboardInputCH.Text;
            strinput[5] = txtSboardOutputCH.Text;
            strinput[6] = digAna;
            strinput[7] = txtProductImage.Text;
            strinput[8] = oprselected;
            strinput[9] = selecteddate;

            nbrinput = 11;

            retval = appcomp.showData(strinput, nbrinput);
        }
    }
}
