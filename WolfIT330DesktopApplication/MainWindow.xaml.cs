﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WolfIT330DesktopApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
            private void newSupplier_Click_1(object sender, RoutedEventArgs e)
        {
            AddSupplier addSupplier = new AddSupplier();
            addSupplier.Show();
        }

        private void newPromotion_Click(object sender, RoutedEventArgs e)
        {
            NewPromotion newPromotion = new NewPromotion();
            newPromotion.Show();
        }

        private void newAmp_Click(object sender, RoutedEventArgs e)
        {
            NewAmp newAmp = new NewAmp();
            newAmp.Show();
        }

        private void newSoundCables_Click(object sender, RoutedEventArgs e)
        {
            NewSoundCable newSoundCable = new NewSoundCable();
            newSoundCable.Show();
        }

        private void newSoundBoard_Click(object sender, RoutedEventArgs e)
        {
            NewSoundBoard newSoundBoard = new NewSoundBoard();
            newSoundBoard.Show();
        }

        private void WirelessMics_Click(object sender, RoutedEventArgs e)
        {
            WirelessMics newWirelessMics = new WirelessMics();
            newWirelessMics.Show();
        }

        private void WiredMic_Click(object sender, RoutedEventArgs e)
        {
            WiredMics newWiredMics = new WiredMics();
            newWiredMics.Show();
        }

        private void ConventionalFixture_Click(object sender, RoutedEventArgs e)
        {
            NewConventionalFixtures newconfix = new NewConventionalFixtures();
            newconfix.Show();
        }

        private void newspeaker_Click(object sender, RoutedEventArgs e)
        {
            Speakers newSpeaker = new Speakers();
            newSpeaker.Show();
        }

        private void ledfixture_Click(object sender, RoutedEventArgs e)
        {
            NewLedFixtures newledfix = new NewLedFixtures();
            newledfix.Show();
        }
    }

   
}
   