﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WolfIT330DesktopApplication
{
    /// <summary>
    /// Interaction logic for NewAmp.xaml
    /// </summary>
    public partial class NewAmp : Window
    {
        public NewAmp()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int retval = 0;

            AppCompute appcomp = new AppCompute();

            string[] strinput = new string[8];
            int nbrinput = 0;

            string oprselected = "";

            if (serviceYes.IsChecked == true)
            { oprselected = "Yes"; }
            else 
            { oprselected = "No"; }

            strinput[0] = txtAmpID.Text;
            strinput[1] = txtAmpBrand.Text;
            strinput[2] = txtAmpModel.Text;
            strinput[3] = txtAmpPower.Text;
            strinput[4] = txtAmpQuantity.Text;
            strinput[5] = txtProductImage.Text;
            strinput[6] = oprselected;
            strinput[7] = txtAmpCreated.Text;
            
            nbrinput = 8;

            retval = appcomp.showData(strinput, nbrinput);
        }
    }
}
