﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WolfIT330DesktopApplication
{
    /// <summary>
    /// Interaction logic for NewSoundCable.xaml
    /// </summary>
    public partial class NewSoundCable : Window
    {
        public NewSoundCable()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int retval = 0;

            AppCompute appcomp = new AppCompute();

            string[] strinput = new string[6];
            int nbrinput = 0;

            string balanced = "";

            if (BalYes.IsChecked == true)
            { balanced = "Yes"; }
            else if (BalNo.IsChecked == true)
            { balanced = "No"; }

            strinput[0] = txtSoundCableID.Text;
            strinput[1] = txtCableType.Text;
            strinput[2] = txtCableLength.Text;
            strinput[3] = txtCableQuantity.Text;
            strinput[4] = balanced;
            strinput[5] = txtCableCreated.Text;
            

            nbrinput = 6;

            retval = appcomp.showData(strinput, nbrinput);
        }
    }
}
